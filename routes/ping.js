var express = require('express');
var router = express.Router();
const pingData = require("ping");
const moment = require("moment")


/* GET users listing. */
router.get('/getChartData', async function (req, res, next) {
    const result = await pingData.promise.probe(req.query.url);
    res.status(200).send({ data: result, timestamp: moment().format('h:mm:ss a')})
});

module.exports = router;
